//  With merge and Compute
package telran.text;

import java.util.HashMap;
import java.util.stream.IntStream;

public class Anagram

{
    public static boolean isAnagramMerge ( String word, String anagram)
    {
        Boolean aBoolean = getBoolean ( word, anagram );
        if ( aBoolean != null ) return aBoolean;

        HashMap<Character, Integer> wordMap = getCharCounts(word);

        int size = anagram.length ( );
        return IntStream.range ( 0, size )
                .map ( i -> wordMap
                .merge ( anagram.charAt ( i ), 1, ( a, b ) -> a - b ) )
                .noneMatch ( merge -> merge < 0 );
    }

    public static boolean isAnagramCompute ( String word, String anagram)
    {
        Boolean aBoolean = getBoolean ( word, anagram );
        if ( aBoolean != null ) return aBoolean;

        HashMap<Character, Integer> wordMap = getCharCounts(word);

        int size = anagram.length ( );
        return IntStream.range ( 0, size )
                .map ( i -> wordMap
                .compute ( anagram.charAt ( i ), ( Character a, Integer b ) -> b!= null ? b - 1 : 1 ) )
                .noneMatch ( compute -> compute < 0 );
    }

    private static HashMap<Character, Integer> getCharCounts ( String string ) {

        HashMap<Character, Integer> res
                = new HashMap<> ( );

        for ( int i = 0; i < string.length ( ); i++ ) {
            res.put ( string.charAt ( i ), res.getOrDefault ( string.charAt ( i ), 0 ) + 1 );
        }

        return res;

    }

    private static Boolean getBoolean ( String word, String anagram ) {

        if (anagram == null || word == null || anagram.length() != word.length()) {
            return false;
        }

        if (word.equals(anagram)) {
            return true;
        }

        return null;
    }
}