//  With merge and Compute
package telran.text;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static telran.text.Anagram.isAnagramCompute;
import static telran.text.Anagram.isAnagramMerge;

class AnagramTests {

    String word = "yellow";

    @Test
    void testAnagramMergeTrue ( ) {
        assertTrue ( isAnagramMerge ( word, "lowley" ) );
        assertTrue ( isAnagramMerge ( word, "woleyl" ) );

        assertTrue ( isAnagramMerge ( word, "llowye" ) );
    }

    @Test
    void testAnagramMergeFalse ( ) {
        assertFalse ( isAnagramMerge ( word, "" ) );
        assertFalse ( isAnagramMerge ( word, "lowlly" ) );

        assertFalse ( isAnagramMerge ( word, "wolleyw" ) );
        assertFalse ( isAnagramMerge ( word, "abcd" ) );
    }

    @Test
    void testAnagramComputeTrue ( ) {
        assertTrue ( isAnagramCompute ( word, "lowley" ) );
        assertTrue ( isAnagramCompute ( word, "woleyl" ) );

        assertTrue ( isAnagramCompute ( word, "llowye" ) );
    }

    @Test
    void testAnagramComputeFalse ( ) {
        assertFalse ( isAnagramCompute ( word, "" ) );
        assertFalse ( isAnagramCompute ( word, "lowlly" ) );

        assertFalse ( isAnagramCompute ( word, "wolleyw" ) );
        assertFalse ( isAnagramCompute ( word, "abcd" ) );
    }

}

